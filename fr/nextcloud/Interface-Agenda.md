# Utilisation de l'application agenda
[<span class="glyphicon glyphicon-arrow-left"></span> Retour à l'accueil](README.md)

Une fois connecté à Framagenda, vous arrivez sur une interface semblable à celle-ci.

![agenda-1](images/agenda-1.png)

La partie gauche de l'application vous montre les contrôles et réglages d'affichage, tandis que les événements s'affichent au centre.

# Vues
Framagenda propose trois modes de vue : la vue « Calendrier » par défaut et les vues « Semaine » et « Jour », qui sont plus proches d'un agenda. Vous pouvez changer cette vue en cliquant sur les boutons correspondants en haut à gauche. De plus, vous pouvez naviguer entre les mois, les semaines et les jours avec les boutons « Précédent » et « Suivant ». Enfin, vous pouvez directement accéder à un jour en cliquant entre ces deux boutons pour afficher une vue réduite du mois, ce qui permet de rapidement accéder à un jour.

![agenda-2](images/agenda-2.png)

# Événements

## Création

Comme un agenda est déjà créé par défaut à la création de votre compte, vous pouvez directement créer un nouvel événement dedans en cliquant dans l'interface, à la date où vous voulez créer l'événement. Si aucun agenda n'existe, il faut en créer un préalablement.

![agenda-3](images/agenda-3.png)

Une fenêtre s'affiche alors pour vous permettre d'entrer des informations de base sur l'événement. Si vous avez besoin de rentrer plus de détails, vous pouvez cliquer sur « Plus… » pour accéder à une interface plus complète.

Vous pouvez valider la création de l'événement en cliquant sur « Créer ». L'événement apparaît dans l'agenda avec la même couleur que l'agenda auquel il est rattaché.

## Édition

En cliquant sur l'événement, vous pouvez éditer ses propriétés. De plus, vous pouvez *glisser-déposer* un événement pour changer sa date ou son horaire facilement.

## Suppression

Vous devez cliquer sur un événement pour pouvoir le supprimer. Supprimer un agenda supprime également tous les événements lui étant rattachés.

## Participants

Vous pouvez ajouter des participants à un événement en entrant leur nom s'ils sont parmi vos contacts, ou bien directement leur adresse de courriel. Une fois les participants validés, ils recevront un courriel avec l'événement en pièce jointe sous la forme d'un fichier .ics.

# Agendas

![agenda-4](images/agenda-4.png)

## Création
Vous pouvez créer un agenda en cliquant sur le bouton susnommé. L'éditeur vous demande un nom et une couleur pour votre agenda.

## Actions

Il est possible de masquer un agenda avec tous ses événemements en cliquant sur le nom de l'agenda. La pastille de couleur à gauche du nom de l'agenda disparait alors et les événements de l'agenda sont masqués.

Le menu situé à côté du nom de l'agenda vous donne accès
* au module d'édition pour modifier le nom ou la couleur d'un agenda,
* au lien *CalDAV* de l'agenda pour la synchronisation avec un client,
* au téléchargement de l'agenda sous forme de fichier iCalendar (*.ics*).
* à la suppression de l'agenda

## Partage
L'icône de partage à côté du nom de l'agenda ouvre le module de partage. Il existe deux types de partages possibles :
* Le partage avec d'autres utilisateurs de Framagenda
* La publication, qui rend votre agenda public

# Partage entre utilisateurs
Il est possible de partager un agenda avec d'autres utilisateurs de Framagenda.

Lorsqu'un agenda est partagé avec un autre utilisateur, celui-ci le voit dans la liste de ses agendas et il a accès aux événements de celui-ci.

Un agenda est partagé par défaut en mode « lecture seule », c'est-à-dire que l'utilisateur avec qui l'agenda est partagé verra les événements mais ne pourra les éditer.
Il est possible également d'autoriser l'écriture pour les bénéficaires du partage : ils peuvent alors modifier des événements dans l'agenda, en créer ou en supprimer.

Note : Contrairement à l'application Fichiers, l'application Agenda ne permet pas (encore) la fédération, ce qui veut dire qu'il n'est pas possible de faire du partage avec des utilisateurs de serveurs différents.

# Publication
On peut également partager un agenda via un lien public, ce qui donne accès à l'agenda pour tous les possesseurs du lien. En accédant au lien, on a accès à l'interface de l'agenda en lecture seule.
De plus, cette interface donne accès à :
* un lien pour télécharger l'agenda au format iCalendar (*.ics*) ;
* un lien *CalDAV* pour ajouter l'agenda à son client de gestion de calendriers favori (voir la partie sur la synchronisation avec des clients) ;
* une adresse *WebDAV* pour pouvoir ajouter l'agenda comme une souscription sur une autre instance NextCloud ou ownCloud.
* un code d'intégration (*iframe*) en HTML pour pouvoir intégrer l'agenda dans une page web comme par exemple Wordpress.

La publication s'effectue en cliquant sur la case à cocher « Partager par lien public », ce qui donne accès au lien public. L'agenda peut également être dépublié de la même manière.

Lorsqu'un agenda a été publié il est possible d'envoyer un courriel à quelqu'un pour lui signaler la publication de ce calendrier.

# Abonnement à des calendriers
Il est possible de s'abonner à des agendas publics sur internet, comme par exemple le calendrier scolaire sur [data.gouv.fr](https://www.data.gouv.fr/fr/datasets/le-calendrier-scolaire/) ou vos horaires de train issus de Captain Train.

Il suffit de coller l'adresse de l'agenda distant dans le champ dédié et de valider. L'agenda est mis dans le cache de votre navigateur et ne sera rafraîchi que toutes les deux heures.

Une fois que l'agenda distant est ajouté, vous voyez tous les événements de cet agenda dans l'interface de l'application en lecture seule.

Note : Les abonnements ne sont pas synchronisables avec un client CalDAV. Sur [Android](Synchronisation/Android.md) il est toutefois possible d'utiliser ICSdroid.
