# Synchronisation avec Jolla / SailFish OS
[<span class="glyphicon glyphicon-arrow-left"></span> Retour à l'accueil](../README.md)

Il faut se rendre dans **Settings** puis **Accounts**, **Add acccount**, puis sélectionner **CalDAV and CardDAV**.

Vous devez alors renseigner :
* Username : l'identifiant de votre compte Framagenda
* Password : le mot de passe de votre compte Framagenda.
* Server address : https://framagenda.org/

Ensuite, il faudra activer/desactiver les services souhaités (e.g. désactiver CardDAV, activer CalDAV pour ne synchroniser que le ou les agendas et pas les contacts).
Pour terminer, accepter.

Il vous est ensuite proposé de configurer en détails la synchronisation.