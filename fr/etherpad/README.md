# Framapad

[Framapad](https://framapad.org) est un éditeur de texte libre est un éditeur de texte collaboratif en ligne.
 Les contributions de chaque utilisateur sont signalées par un code couleur,
 apparaissent à l’écran en temps réel et sont enregistrées au fur et à mesure qu’elles sont tapées.

Le service repose sur le logiciel libre [Etherpad](http://etherpad.org/).

---

## Tutoriel vidéo

<div class="text-center">
  <p><video width="420" height="340" controls="controls" preload="none" poster="https://framatube.org/images/media/992l.jpg">
      <source src="https://framatube.org/blip/framapad_1.mp4" type="video/mp4" />
      <source src="https://framatube.org/blip/framapad_1.webm" type="video/webm" />
  </video></p>
  <p>→ La <a href="https://framatube.org/blip/framapad_1.webm">vidéo au format webm</a></p>
</div>

Vidéo réalisée par Frédéric Véron, professeur de SVT dans l’Académie de Créteil.