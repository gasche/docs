# Je consulte une carte uMap

## Ce que nous allons apprendre

*  Manipuler une carte uMap
*  Partager une carte uMap
*  Connaître les principales fonctionnalités de uMap

## Procédons par étapes

### 1. Manipuler la carte

Vous avez reçu par mail un lien vers une carte umap. Voici les principaux éléments de la carte, et les opérations disponibles pour la manipuler. La carte umap représentée ci-dessous est disponible [ici](http://umap.openstreetmap.fr/fr/map/festival-des-3-continents_26381).

![Composants d'une carte umap](images/anatomie_carte_umap.jpg)

----
À droite de la carte et selon le choix de son auteur peut être affiché un des deux panneaux suivants :

*  **Légende** : le titre de la carte, une description éventuelle, et la liste des calques
*  **Visualiser les données** : l'ensemble des éléments de la carte, répartis par calques (voir plus bas)
Le panneau Légende peut être affiché en cliquant sur le mot "Légende", toujours visible en bas à droite de la carte.

Comme pour la plupart des cartes interactives vous pouvez :

*  déplacer la carte par un glisser-déposer
*  effectuer zooms avant et arrière avec les boutons + et -, ou avec la molette de la souris
*  sélectionner un élément de la carte par un clic de la souris : apparaît alors une fenêtre *popup* affichant une description de l'élément. Celle-ci peut inclure du texte, une image, un lien vers un site Web. Dans notre exemple la description de chaque cinéma contient une image qui est un lien sur le site Web du cinéma.

**Remarque** : les boutons en haut à gauche de la carte, ainsi que la barre de légende, peuvent ne pas être disponibles si l'auteur de la carte a choisi de les cacher.

Voyons maintenant quelques fonctionnalités propres à umap.

### 2. Le sélecteur de calques

Les éléments d'une carte umap peuvent être répartis dans plusieurs calques, ou couches. Cela permet de structurer une carte, pour qu'elle soit plus claire et plus facile à maintenir. L'utilisateur peut choisir d'afficher ou cacher chaque calque individuellement.

![Le sélecteur de calques](images/umap_calques.png) Le sélecteur de calques est l'icône visible en haut à gauche de la carte sous les boutons de zoom. Lorsque vous positionnez la souris sur ce bouton, la liste des calques apparaît, vous pouvez alors afficher ou cacher chaque calque, ou encore centrer la carte sur le contenu d'un calque.
![](images/umap_sélecteur_calques.png)

Dans cet exemple le calque "Stations Bicloo" est caché : cliquer sur l'oeil de ce calque permet de l'afficher.
La liste des calques, avec éventuellement un descriptif de chaque calque, est aussi visible dans la légende de la carte.

### 3. Le bouton Plus

![Plus d'outils](images/umap_plus.png) Sous le sélecteur de carte est visible un bouton portant le texte "Plus". Un clic sur ce bouton fait apparaître une série de boutons.


*  ![Zoom sur une localité](images/umap_search.png) permet de chercher une localité et de centrer la carte dessus : saisissez le nom d'une commune et tapez sur ''Entrée''.
*  ![Voir en plein écran](images/umap_full_screen.png) place le navigateur en mode plein écran, que l'on peut quitter avec le même bouton ou avec la touche ''Échap'' du clavier.
*  ![Exporter et partager la carte](images/umap_share.png) permet de partager la carte ou d'en exporter les données. Un panneau à droite de la carte est affiché, il est expliqué ci-dessous.
*  ![Centrer la carte sur votre position](images/umap_geoloc.png) permet de vous géolocaliser((La géolocalisation exige de demander l'autorisation de l'utilisateur, votre navigateur Web peut donc vous demander d'accepter ou activer la géolocalisation)), c'est-à-dire centrer la carte sur votre position actuelle.
*  ![Changer le fond de carte](images/umap_layers.png) affiche à droite plusieurs fonds de carte : cliquer sur l'un d'eux change le fond de la carte.
*  ![Ouvrir cette carte dans un éditeur OpenStreetMap pour améliorer les données](images/umap_edit_osm.png) est utile pour améliorer la carte OpenStreetMap - ce qui sort de l'objet de ce tutoriel.
*  ![Mesurer les distances](images/umap_measure.png) est un outil de mesure. Activer cet outil a deux effets : d'une part il affiche la longueur des éléments linéaires de la carte et l'aire des éléments surfaciques ; d'autre part il vous permet de tracer sur la carte une ligne dont la longueur est affichée. Cliquez à nouveau sur le bouton pour désactiver cet outil.

#### Partager la carte

Le panneau de partage de la carte offre trois possibilités. Votre choix dépend de la manière dont vous souhaitez partager la carte :

*  **URL courte** permet de copier une URL abrégée - équivalente à l'URL de la carte - que vous pouvez par exemple envoyer dans un mail.
*  **Embarquer la carte en iframe** permet d'inclure la carte dans une page Web : il suffit de copier le code HTML et de l'insérer dans celui de votre page Web. Cette possibilité est explorée en détails dans le tutoriel [Je publie ma carte et en contrôle l'accès](7-publication-et-droits-d-acces.html).
*  **Télécharger les données** permet d'obtenir les données visibles sur la carte, dans différents formats. Cela peut vous permettre d'utiliser ces données avec un autre outil.

![](images/umap_donnees.jpg)

### 4. Visualiser les données

La liste des éléments de la carte peut être affichée avec un clic sur **Visualiser les données**, accessible depuis le sélecteur de calques, la barre de légende, ou encore en haut du panneau Légende.

Le panneau alors visible à droite montre l'ensemble des éléments de la carte, organisés par calques. La loupe à gauche de chaque élément permet d'afficher sur la carte la popup décrivant cet élément. Le texte de saisie au-dessus de la liste permet de rechercher un élément, en ne montrant que ceux dont le nom contient le texte saisi.

----
## Faisons le point

Ce premier tutoriel nous a permis de découvrir les principales fonctionnalités d'une carte uMap. Nous allons maintenant [apprendre à créer une telle carte](2-premiere-carte.html).



