# Création d'équipes


___

De nouvelles équipes peuvent être créées si l'administrateur système a mis *Autorisation de création d'équipes* à *vrai* depuis la Console système.

## Méthodes pour créer une équipe

Les équipes peuvent être créées depuis le menu principal, la page d'accueil du système ou encore la page d'identification de l'équipe.

#### Menu principal

Cliquer sur le menu **Trois points** dans Mattermost, puis sélectionner **Créer une nouvelle équipe**. Si cette option n'est pas visible dans le menu, alors l'administrateur système a mis *Autorisation de création d'équipes* à *faux*.


#### Page d'accueil du système

Allez a l'adresse web de votre système, `https://domain.com/`. Entrez une adresse de courriel valide et cliquez sur **Créer une équipe** pour être guidé pour les étapes de configuration suivantes. Si cette option n'est pas visible dans la page internet, alors l'administrateur système a mis *Autorisation de création d'équipes* à *faux*. Il n'est pas nécessaire d'avoir un compte existant sur le système pour créer une équipe depuis la page d'accueil du système.

#### Page d'identification de l'équipe


Allez a l'adresse web de votre équipe, `https://domain.com/teamurl/`. Si vous êtes connecté, l'adresse web va ouvrir Mattermost et vous pourrez créer une nouvelle équipe depuis le menu principal. Si vous n'êtes pas connecté, l'adresse web vous redirigera vers la page d'identification où vous pourrez **Créer une nouvelle équipe**. Si cette option n'est pas visible sur la page, alors l'administrateur système a mis *Autorisation de création d'équipes* à *faux*.  Il n'est pas nécessaire d'avoir un compte existant sur le système pour créer une équipe depuis la page d'identification.

## Nom d'équipe et sélection de l'URL

Il y a quelques détails et restrictions à prendre en compte quand on choisit un nom d'équipe et une URL d'équipe.

#### Nom d'équipe 

Il s'agit du nom qui sera affiché dans les menus et les en-têtes. 

- Il peut contenir n'importe quelle lettre, nombre ou symbole.

- Il est sensible à la casse.

- Il doit comprendre entre 4 et 15 caractères.

#### URL d'équipe

L'URL de l'équipe est une partie de l'adresse web qui renvoie vers votre équipe sur le domaine du système, `https://domain.com/teamurl/`. 

- Elle peut contenir uniquement des lettres minuscules et non-accentuées, des nombres et des tirets.

- Elle doit commencer avec une lettre et ne peut pas finir avec un tiret. 

- Elle doit comprendre entre 4 et 15 caractères.

Si l'administrateur système a activé *Restriction des noms d'équipes*, l'URL de l'équipe ne peut pas commencer avec les mots suivants : www, web, admin, support, notify, test, demo, mail, team, channel, internal, localhost, dockerhost, stag, post, cluster, api, oauth.


## Rôles des utilisateurs appartenant à plusieurs équipes

Chaque utilisateur est distinct et appartient à une équipe. Le créateur d’une équipe reçoit automatiquement les droits « Administrateur d’équipe » pour cette équipe, même s’il est administrateur système dans une autre équipe. Un administrateur système qui a des comptes dans plusieurs équipes doit donner les droits « Administrateur système » à tous ses comptes, depuis la console système. Pour cela, allez dans **Menu principal > Console système**, puis cliquez sur **Utilisateurs** sous l’en-tête *Équipes* pour l’équipe que vous voulez gérer.




